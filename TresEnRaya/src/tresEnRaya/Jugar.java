package tresEnRaya;

import java.util.Scanner;

public class Jugar {


	static Scanner src = new Scanner(System.in);
	
	public static String jugarPartida(String j1, String j2) {
		
		final char FICHA_Jugador1 = 'X', FICHA_Jugador2 = 'O';   
		final int N = 3;						     
		String ganador = null;						
		char [][] tablero;							 
		int turno;									
		boolean finPartida;						 	 
		int estado = -1;									
		finPartida = false;
		turno = 1;									
		tablero = inicializarTablero(N);             
		
		
		while (!finPartida) {
			mostrar(tablero,turno, N , j1, j2);
			tirada(tablero,turno, N ,j1, j2);			
			estado = comprobarTablero(tablero);
			finPartida = (estado != -1);
			turno = siguienteJugador(turno);
		}
		ganador = resultadoPartida(tablero,N, estado);
		return ganador;  
	}

	private static char[][] inicializarTablero(int n) {
		
		
		return null;
	}

	private static String resultadoPartida(char[][] tablero,int n, int estado) {
		
		return null;
	}

	private static int siguienteJugador(int turno) {
		if (turno ==1) return 2;
		
		return 0;
	}

	private static int comprobarTablero(char[][] tablero) {
		
		return 0;
	}

	private static void tirada(char[][] tablero, int turno,int n, String j1, String j2) {
		
		
	}

	private static void mostrar(char[][] tablero, int turno,int n, String j1, String j2) {
		
		
	}
}
	
