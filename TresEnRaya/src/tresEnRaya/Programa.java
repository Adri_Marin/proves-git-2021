package tresEnRaya;

import java.util.Scanner;

public class Programa {
	
	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
	
		int opcio;
		String jugador1 = "";
		String jugador2 = "";
		Jugador jugadores = new Jugador();
		boolean definido = false;
		String ganador;
		
		do {
			opcio = menu();
			switch (opcio) {
				case 1: Ajuda.mostrarInfo();
						break;
				case 2: jugador1 = Jugador.definirJugador();
						jugador2 = Jugador.definirJugador();
						definido = true;
						break;
				case 3: if (definido) {
							ganador = Jugar.jugarPartida(jugador1, jugador2);
							if (ganador != null) {
								if (ganador.equals(jugador1)) {
									Jugador.actualizar(jugador1, "gana");
									Jugador.actualizar(jugador2, "pierde");
								}
								else{
									Jugador.actualizar(jugador2, "gana");
									Jugador.actualizar(jugador1, "pierde");
								}
							}
							definido = false;
						}
						else System.out.println("Error, cal definir primer el jugador");
						break;
				case 4: Jugador.mostrarJugadores();
						break;
				case 0: System.out.println("Moltes gr�cies per jugar\n\nAdeu!!!");
						break;
				default: System.out.println("Error, opci� incorrecta. Torna-ho a provar..");
			}
			
		} while (opcio != 0);
	}
	
	private static int menu() {
		System.out.println("1.- Mostrar Ajuda");
		System.out.println("2.- Definir Jugador");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.  Puntuacions");
		System.out.println("0.- Sortir");
		System.out.println("\n Escull una de les seg�ents opcions: ");
		
		return leerEntero();
		
	}
	private static int leerEntero() {
		int op = 0;
		boolean valid = false;
		
		do {
			try {
				op = src.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un n�mero enter");
				src.nextLine();
			}
		} while(!valid);
		
		return op;
	}

}

