import java.util.Random;
import java.util.Scanner;

public class Programa {
		   static Scanner src = new Scanner(System.in);
		   
		   //La clase Main desenvolupa les  diferents opcions del joc del buscamines.
		   public static void main(String[] args) {
			   
		        Scanner sc = new Scanner(System.in);
		        
		        int opcion;
		        String jugador = "";
		        Jugador jugadores = new Jugador();
		        boolean definido = false;
		        boolean ganador;
		        int nivel;
		        int filas = 0;
		        int cols = 0;
		        int minas = 0;
		        
		        do {
		            opcion = menu();
		            System.out.println("Escull una opci�");
		            opcion = sc.nextInt();
		            switch (opcion) {
		            
		            	case 1:  
		            	Ajuda.mostrarInfo();
						break;
						
		            	case 2:
					
						jugador = Jugador.definirJugador();
							nivel = Opcions.obtenerNivelJuego();
							filas = Opcions.obtenerTama�o(nivel, "filas");
							cols = Opcions.obtenerTama�o(nivel, "columnas");
							minas = Opcions.numeroMinas(nivel, filas, cols);
							definido = true;
							break;

		                case 3: if (definido) {
		                	
		                	ganador = Jugar.jugarPartida(jugador, filas, cols, minas);
							if (ganador) 
									Jugador.actualizar(jugador, "gana");
							else
									Jugador.actualizar(jugador, "pierde");
							definido = false;
						}
						else System.out.println("Error, cal definir primer el jugador");
						break;
  
		                case 4:
		                	Jugador.mostrarJugador(jugador);
							break;
		                            
		            	case 0: 
		            		System.out.println("Moltes gr�cies per jugar\n\nAdeu!!!");
							break;
		            	default: System.out.println("Error, opci� incorrecta. Torna-ho a provar..");
			}
			
		                    
		        }while (opcion != 0);
		   }
		            
		            private static int menu() {
		        		// TODO Auto-generated method stub
		        		System.out.println("1.- Mostrar Ajuda");
		        		System.out.println("2.- Opcions");
		        		System.out.println("3.- Jugar Partida");
		        		System.out.println("4.- Veure rankings");
		        		System.out.println("0.- Sortir");
		        		System.out.println("\nEscull una opci�: ");
		        		
		        		return leerEntero();
		        		
		        	}
		            private static int leerEntero() {
		        		// TODO Auto-generated method stub
		        		int op = 0;
		        		boolean valid = false;
		        		
		        		do {
		        			try {
		        				op = src.nextInt();
		        				valid = true;
		        			}
		        			catch (Exception e) {
		        				System.out.println("Error, has de posar un n�mero enter");
		        				src.nextLine();
		        			}
		        		} while(!valid);
		        		
		        		return op;
		        		
		        		
		        	}
		            
		        }    
		   
		           
		        
		   