import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {
		
		static Scanner src = new Scanner(System.in);
		static ArrayList<String> nomJugadors = new ArrayList<String>();
		static ArrayList<Integer> partidasGanadas = new ArrayList<Integer>();
		static ArrayList<Integer> partidasPerdidas = new ArrayList<Integer>();
		
		public static String definirJugador() {
			// TODO Auto-generated method stub
			
			String nom; 
			System.out.print("Introduce tu nombre como jugador: ");
			nom = src.nextLine();
			if (nomJugadors.indexOf(nom) == -1) { //El jugador no existe
				nomJugadors.add(nom);
				partidasGanadas.add(0);
				partidasPerdidas.add(0);
			}
			
			return nom;
		}

		public static void mostrarJugador(String jugador) {
			// TODO Auto-generated method stub
			int i = nomJugadors.indexOf(jugador);
			
			if (i == -1) 
				System.out.println("Error, aquest jugador no existeix");
			else {
				System.out.println("\nDades del jugador " + nomJugadors.get(i) + "\n");
				System.out.println("Partides guanyades: " + partidasGanadas.get(i));
				System.out.println("Partides perdudes: " + partidasPerdidas.get(i) + "\n");
			}
		}

		public static void mostrarJugadores() {
			for (int i = 0; i < nomJugadors.size(); i++) {
				System.out.println("Dades del jugador " + nomJugadors.get(i) + "\n");
				System.out.println("Partides guanyades: " + partidasGanadas.get(i));
				System.out.println("Partides perdudes: " + partidasPerdidas.get(i) + "\n");
			}
		}

		public static void actualizar(String jugador, String estado) {
			// TODO Auto-generated method stub
			int i = nomJugadors.indexOf(jugador);
			
			if (i == -1) 
				System.out.println("Error, aquest jugador no existeix");
			else {
				if (estado.contentEquals("gana")) 
					partidasGanadas.set(i, partidasGanadas.get(i)+1);
				else 
					partidasPerdidas.set(i, partidasPerdidas.get(i)+1);
			}
		}

		

	}
