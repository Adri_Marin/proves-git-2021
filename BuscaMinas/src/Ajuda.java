public class Ajuda {

	public static void mostrarInfo() {
		
		System.out.println("****  Instruccions Buscamines  ******");
		System.out.println("----------------------------------------------");
		System.out.println("El joc del buscamines es basa en un tauler de mida variable ones col�locen una certa quantitat de mines aleat�riament.\n");
		System.out.println("Participa un jugador, descobrint una casella cada torn.\n");
		System.out.println("La casella a destapar pot contenir 3 tipus de valors:\n");
		System.out.println("Mina: el programa finalitza i la partida s�ha perdut.\n");
		System.out.println("N�mero entre 1 i 8: indica quantes caselles contig�es contenen una mina. El jugador pot continuar jugant.\n");
		System.out.println("Buit (aigua): Casella que no t� cap mina al seu voltant. Al destaparla provoca que es destapin autom�ticament totes les caselles contig�es de forma ramificada fins que arriba a caselles on hi hagi un n�mero.\n");
		System.out.println("El joc acaba quan les �niques caselles sense descobrir siguin les mines, o b� quan el jugador descobreix una mina (BOOM!!!).\n");
		System.out.println("Molta sort!!!\n");
		System.out.println("-----------------------------------------------");
		
	}


}
