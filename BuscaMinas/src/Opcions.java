import java.util.Scanner;

public class Opcions {
static Scanner src = new Scanner(System.in);
	
	public static int obtenerNivelJuego() {
		// TODO Auto-generated method stub
		int nivel = 0;
		boolean correcto = false;
		
		System.out.println("\n****  Benvingut al joc del Buscamines  ****\n");
		System.out.println("Indica el nivell de joc: ");
		System.out.println("1. Principiant");
		System.out.println("2. Intermig");
		System.out.println("3. Expert");
		System.out.println("4. Personalitzat");
		System.out.print("\n Opci�? : ");
		
		do {
			nivel = leerEntero();
			if (nivel >= 1 && nivel <= 4) correcto = true;
			else System.out.println("Error, opci� de nivell incorrecta. Torna a probar");
		} while (!correcto);
		
		return nivel;
	}

	public static int leerEntero() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;
		
		do {
			try {
				op = src.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un n�mero enter");
				src.nextLine();
			}
		} while(!valid);
		
		return op;
	}
	public static int obtenerTama�o(int nivel, String mensaje) {
		// TODO Auto-generated method stub
		/*
		 * Nivel principiante: 8  8 casillas y 10 minas.
		 * Nivel intermedio: 16  16 casillas y 40 minas.
		 * Nivel experto: 16  30 casillas y 99 minas.
		 * Nivel personalizado: en este caso el usuario personaliza su juego eligiendo el nmero de minas
		 *  y el tamao de la cuadricula
		 */
		
		int tam = 0;
		
		switch (nivel) {
			case 1: tam = 8;
					break;
			case 2: tam = 16;
					break;
			case 3: if (mensaje.equalsIgnoreCase("files")) tam = 16;
					else tam = 30;
					break;
			case 4: boolean correcto = false;
					do {
						System.out.print("Indica el n�mero de " + mensaje + " en el teu tabler personalitzat");
						tam = leerEntero();
						if (tam >= 1 && tam <= 100) correcto = true;
						else System.out.println("Error, n�mero de " + mensaje + " incorrecte. El m�xim perm�s �s 100. Torna a probar");
					} while (!correcto);
		}
		return tam;
	}

	public static int numeroMinas(int nivel, int filas, int cols) {
		// TODO Auto-generated method stub
		int minas = 0;
		switch (nivel) {
			case 1: minas = 10;
					break;
			case 2: minas = 40;
					break;
			case 3: minas = 99;
					break;
			case 4: boolean correcto = false;
					int max = (int)((filas * cols) * 0.2);      //el mximo de minas que permitimos es un 20% del total de casillas del tablero
					do {
						System.out.print("Indica el n�mero de mines en tu tabler personalitzat");
						minas = leerEntero();
						if (minas >= 1 && minas <= max) correcto = true;
						else System.out.println("Error, n�mero de mines incorrecte . Torna a probar");
					} while (!correcto);
	}
	return minas;
	}

}


