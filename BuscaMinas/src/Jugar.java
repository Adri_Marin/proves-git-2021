import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugar {
	
	static Scanner src = new Scanner(System.in);
	final static char bomba = '*';
	final static char agua = '_';
	final static char tapado = 'O';

    //�s el random per generar la posici� de les mines autom�ticament.
	static Random rand = new Random();
	
	static String jugador;
	
	static int victoria = 0;
	
	static int[] mides = {0,0,0};
	
	public static boolean jugarPartida(String jugador, int filas, int cols, int minas) {
		// TODO Auto-generated method stub
		
		char [][] visible = null;
		char [][] oculto = null;
		int estado;
		
		boolean finPartida;
		int totalMinas;
		
		visible = iniciarTablero(filas, cols, visible);
		oculto = ponerMinas(filas,cols,minas, oculto);
		estado =1;
		minas = 0;
		finPartida = false;
		
		while (!finPartida) {
			mostrar(oculto, visible, filas, cols, minas);
			estado = tirada (visible, oculto,filas, cols);
			finPartida = estado ==1;
		}
		resultadoPartida(visible,oculto,filas,cols,estado);
		
		return estado == 0;
		
	}
		
		
	
		private static char[][] ponerMinas(int filas, int cols, int minas, char  [][] oculto) {
			int a = 0;
			int b = 0;
			int z = 0;
			for(int i = 0; i < mides[0]; i++) {
				for(int x = 0; x < mides[1]; x++) {
					oculto[i][x] = tapado;
					
				}
			}
	while (z < mides[2]) {
				a = rand.nextInt(mides[0]);
				b = rand.nextInt(mides[1]);
				if(oculto[a][b] == tapado) {
					oculto[a][b] = '*';
					z++;
				}
			}
	return oculto;
	}



		private static int tirada(char[][] visible, char[][] oculto, int filas, int cols) {
		// TODO Auto-generated method stub
		return 0;
	}



		private static void resultadoPartida(char[][] visible, char[][] oculto, int filas, int cols, int estado) {
		// TODO Auto-generated method stub
		
	}



		private static int tirada (char[][] visible, char [][] oculto, int filas, int cols, int minas) {
			
		int fila, col;
		char valor;
		int estado;
		
		fila = nuevoNumero(filas, "Introdueix un valor per la fila:");
		col = nuevoNumero(cols, "Introdueix un valor per la columna:");
		visible = mostrar(oculto, visible, filas, cols, minas);
		valor = oculto [fila][col];
		
		estado = partidaAcabada(valor, visible);
		
		return estado;
		
		}
		
		private static int nuevoNumero(int cols, String string) {
			// TODO Auto-generated method stub
			return 0;
		}

		private static int partidaAcabada(char valor, char[][] visible) {
			// TODO Auto-generated method stub
			return 0;
		}

		static char[][] iniciarTablero(int filas, int cols, char [][]  oculto) {
			for(int i = 0; i < mides[0]; i++) {
				for(int x = 0; x < mides[1]; x++) {
					oculto[i][x] = tapado;		
				}
			}
			return oculto;
		}
			
		
		
		static int destapar(int filas, int cols, char[][] oculto){
			int cont = 0; //Variable que serveix per guardar el valor de la proximitat on es trobi la mina.
			int[] columnes = new int[] { 1, -1, 0, 0, 1, 1, -1, -1 }; //Vector que serveix per tenir guardar les 8 posicions possibles a comprovar a les columnes.
			int[] files = new int[] { 0, 0, 1, -1, 1, -1, -1, 1 }; //Vector que serveix per tenir guardar les 8 posicions possibles a comprovar a les files.
					cont = 0;
					for (int z = 0; z < 8; z++) {
						int sumfiles = cols + files[z];
						int sumcolumnes = cols + columnes[z];
						if (sumfiles < oculto.length && sumfiles >= 0 && sumcolumnes < oculto[0].length && sumcolumnes >= 0) {
							if (oculto[sumfiles][sumcolumnes] == '*') {
								cont++;
							}
						}
			}
			return cont;
		}
		
		static char[][] mostrar(char [][] visible,char [][] oculto, int filas,int cols, int minas) {
				int n = destapar(filas,cols,oculto); 
				visible[filas][cols] = Integer.toString(n).charAt(0);
				if (visible[filas][cols] != '0') {
					return visible;
				}
				visible[cols][filas] = Integer.toString(n).charAt(0);
				int[] columnes = new int[] { 1, -1, 0, 0, 1, 1, -1, -1 };
				int[] files = new int[] { 0, 0, 1, -1, 1, -1, -1, 1 };
				for (int z = 0; z < 8; z++) {
					int sumfiles = cols + files[z];
					int sumcolumnes = filas + columnes[z];
					if (sumfiles < oculto.length && sumfiles >= 0 && sumcolumnes < oculto[0].length && sumcolumnes >= 0) {
						if (visible[sumfiles][sumcolumnes] == '9') {
							mostrar(oculto, visible, sumfiles, sumcolumnes, minas);
						}
					}
				}
				return visible;
		
	}

}
