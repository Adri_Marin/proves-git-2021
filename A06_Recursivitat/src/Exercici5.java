//Funci� que permet sumar els d�gits d�un n�mero n positiu que rebem per par�metre. 
//No hem de comprovar que n sigui positiu.
import java.util.Scanner;

public class Exercici5 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numero;
		int resultado = 0;
		System.out.println("Introduce un n�mero para sumar sus digitos:");
		numero = lector.nextInt();
		resultado = Suma(numero);
		System.out.println(resultado);

	}
	public static int Suma(int numero) {
		if (numero == 0)
			return 0;
		else {
			int digit = numero%10;
			numero = numero/10;
			return digit + Suma(numero);
		}
			
	}

}


