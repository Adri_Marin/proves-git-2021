//Funci� que calcula el producte de dos n�meros que rep per par�metre mitjan�ant 
//el algorisme rus. 
//Aquest algorisme diu que donat dos n�meros (n, m) per multiplicar-los hem d'anar 
//calculant de n el seu doble 
//i de m la seva meitat entera. Finalitza el proc�s quan  arriba a 1. Llavors, hem 
//de sumar els n�meros generats 
//de n on el valor corresponent de m sigui senar. Aquest resultat �s la multiplicaci� n*m.

import java.util.Scanner;

public class Exercici9 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numero;
		int numero2;
		int resultado;
		
		
		System.out.println("Introduce 1r numero:");
		numero = lector.nextInt();
		System.out.println("Introduce 2n numero:");
		numero2 = lector.nextInt();
		
		resultado = multiplicaRus(numero,numero2);
		System.out.println("El resultado es:"+resultado);

	}
	public static int multiplicaRus(int numero, int numero2) {
		if(numero2 == 1)
			return numero;
		else if(numero2%2!=0) {
			return numero + multiplicaRus(numero*2,numero2/2);
		}
		else {
			return multiplicaRus(numero*2,numero2/2);
		}
		
	}

}
