import java.util.Scanner;

//Funci� que rep dos n�meros enters (n, x) i calcula nx. 

public class Exercici7 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numero;
		int elevar;
		int resultado = 0 ;
		
		System.out.println("Introduce numero para elevar:");
		numero = lector.nextInt();
		System.out.println("Introduce el valor al que desea elevar:");
		elevar = lector.nextInt();
		
		resultado = elevar(numero,elevar);
		System.out.println(resultado);

	}
	
	public static int elevar(int numero, int elevar) {
		if( elevar == 0)
			return 1;
		else {
			return numero * elevar(numero,elevar-1);
			
		}
		
	}

}

