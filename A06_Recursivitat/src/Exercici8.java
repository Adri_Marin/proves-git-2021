//Funci�  que calculi el m�xim com� divisor de dos valors segons l'algorisme d'Euclides.
import java.util.Scanner;

public class Exercici8 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numero;
		int divisor;
		int resultado;
		
		System.out.println("Introduce 1r numero:");
		numero = lector.nextInt();
		System.out.println("Introduce 2o numero:");
		divisor = lector.nextInt();
		
		resultado = elevar(numero,divisor);
		System.out.println("El mcd es:"+resultado);

	}
	
	public static int elevar(int numero, int divisor) {
		int residuo = numero%divisor;
		if( residuo == 0)
			return divisor;
		else {
			return elevar(divisor,residuo);
			
		}
		
	}

}

