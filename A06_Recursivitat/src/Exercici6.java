import java.util.Scanner;

//Funci� que  sumi els valors dels elements d�un vector d�enters que rep per par�metre.

public class Exercici6 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int resultado = 0;
		int x = 0;
		int[] vector = new int[]{2,10,5,7};
		
		resultado = SumaVector(vector,x);
		System.out.println(resultado);

	}
	public static int SumaVector(int [] vector,int x) {
		if (x>vector.length-1)
			return 0;
		else {
			return vector[x] + SumaVector(vector,x+1);
			
		}
			
	}
}