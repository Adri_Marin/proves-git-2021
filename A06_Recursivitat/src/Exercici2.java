import java.util.Scanner;

//Funci� que calculi una aproximaci� al valor del n�mero e com la suma de la s�rie:

public class Exercici2 {

	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numeroFactorial;
		double resultado;
		System.out.println("Introduce numero factorial:");
		numeroFactorial = lector.nextInt();
		resultado = sumFactorial(numeroFactorial);
		System.out.println(resultado);
	}
	
	public static double sumFactorial( int numeroFactorial) {
		if(numeroFactorial == 1)
			return 2;
		else
			return 1.0/factorial(numeroFactorial) + sumFactorial(numeroFactorial-1);
	
	}
	public static int factorial(int numeroFactorial) {
		if(numeroFactorial == 1 || numeroFactorial == 0) {
			return 1;
		}else {
			return numeroFactorial * factorial(numeroFactorial-1);
		}
		
	}
}