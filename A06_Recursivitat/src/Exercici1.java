//Funci� que calculi el factorial d�un n�mero enter positiu passat per par�metre.

import java.util.Scanner;


public class Exercici1 {

	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numeroFactorial;
		int resultado;
		System.out.println("Introduce numero factorial:");
		numeroFactorial = lector.nextInt();
		resultado = factorial(numeroFactorial);
		System.out.println(resultado);
	}
	
	public static int factorial(int numeroFactorial) {
		if(numeroFactorial == 1 || numeroFactorial == 0) {
			return 1;
		}else {
			return numeroFactorial * factorial(numeroFactorial-1);
		}
		
	}
	

}
