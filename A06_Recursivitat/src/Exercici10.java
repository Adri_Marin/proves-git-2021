//Funci� que a partir d�un n�mero binari calculi el seu valor en decimal.

import java.util.Scanner;

public class Exercici10 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numero;
		double resultado = 0;
		int posicio = 0;	
		
		System.out.println("Introduce el numero en binario:");
		numero = lector.nextInt();
		resultado = binariAdecimal(numero,posicio);
		System.out.println("El resultado es:"+resultado);
	}
	public static double binariAdecimal(int numero,int posicio) {
		
		if(Integer.toString(numero).length()==1)
			return Math.pow(2,posicio)*numero;
		else
			return Math.pow(2,posicio)*numero%10 + binariAdecimal(numero/10, posicio+1);
	}
}

