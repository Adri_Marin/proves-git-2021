import java.util.Scanner;

//Funci� que calculi la divisi� entera de dos n�meros enters positius mitjan�ant restes successives. 

public class Exercici3 {
	public static Scanner lector = new Scanner(System.in);

	public static void main(String[] args) {
		int numeroDividir;
		int divisor;
		int resultado = 1 ;
		
		System.out.println("Introduce numero a dividir:");
		numeroDividir = lector.nextInt();
		System.out.println("Introduce el divisor:");
		divisor = lector.nextInt();
		
		resultado = dividir(numeroDividir,divisor);
		System.out.println(resultado);

	}
	public static int dividir(int numeroDividir,int divisor) {
		if(numeroDividir == 0)
			return 0;
		else {
			return dividir(numeroDividir-divisor, divisor)+1;
			
		}
		
	}

}

